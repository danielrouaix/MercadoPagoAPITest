//
//  PayerCosts.swift
//  Pagos
//
//  Created by Daniel Dalto on 15/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import Foundation

class PayerCosts: Codable {
    var paymentMethodId: String?
    var cardIssuer: CardIssuer?
    var costs: [Installment] = []

    enum CodingKeys: String, CodingKey {
        case paymentMethodId = "payment_method_id"
        case cardIssuer = "issuer"
        case costs = "payer_costs"
    }
}
