//
//  MPError.swift
//  Pagos
//
//  Created by Daniel Dalto on 16/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import Foundation

/*{
    "message": "invalid public_key",
    "error": "not_found",
    "status": 404,
    "cause": [
    {
    "code": "1001",
    "description": "public_key not found"
    }
    ]
}*/

class MPError: Codable {
    var message: String?
    var error: String?
    var status: Int?
    var cause: [MPErrorCause]?

    enum CodingKeys: String, CodingKey {
        case message = "message"
        case status = "status"
        case error = "error"
        case cause = "cause"
    }

    func stdError() -> NSError {
        if let firstCause = cause?.first {
            return NSError(domain: "MercadoPagoAPI", code: Int(firstCause.code ?? "-1") ?? -1, userInfo: [NSLocalizedDescriptionKey: firstCause.description])
        } else {
            return NSError(domain: "MercadoPagoAPI", code: status ?? -1, userInfo: [NSLocalizedDescriptionKey: (message ?? "API Error") ])
        }
    }
}

class MPErrorCause: Codable {
    var code: String?
    var description: String = "Unknown"

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case description = "description"
    }
}
