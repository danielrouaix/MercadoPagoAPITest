//
//  PaymentType.swift
//  Pagos
//
//  Created by Daniel Dalto on 15/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import Foundation

class PaymentType: Codable {
    var paymentId: String?
    var paymentTypeId: String?
    var name: String?
    private var secureThumbnail: String?
    private var _thumbnail: String?

    enum CodingKeys: String, CodingKey {
        case paymentId = "id"
        case paymentTypeId = "payment_type_id"
        case name = "name"
        case secureThumbnail = "secure_thumbnail"
        case _thumbnail = "thumbnail"
    }

    // Prefer secure connection if available
    var thumbnail: URL? {
        if let th = secureThumbnail, let thUrl = URL(string: th) {
            return thUrl
        } else if let th = _thumbnail {
            return URL(string: th)
        }
        return nil
    }
}

func == (lhs: PaymentType, rhs: PaymentType) -> Bool {
    return lhs.paymentId == rhs.paymentId
}
