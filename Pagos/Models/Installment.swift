//
//  Installments.swift
//  Pagos
//
//  Created by Daniel Dalto on 15/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import Foundation

class Installment: Codable {
    var installments: Int?
    var installmentRate: Double?
    var discountRate: Double?
    var labels: [String]?
    var installmentRateCollector: [String]?
    var minAllowedAmount: Double?
    var maxAllowedAmount: Double?
    var recommendedMessage: String?
    var installmentAmount: Double?
    var totalAmount: Double?

    var cft: String? {
        var value: String?
        labels?.forEach({ (str) in
            if str.uppercased().contains("CFT") {
                value = str
            }
        })
        return value
    }

    enum CodingKeys: String, CodingKey {
        case installments = "installments"
        case installmentRate = "installment_rate"
        case discountRate = "discount_rate"
        case labels = "labels"
        case installmentRateCollector = "installment_rate_collector"
        case minAllowedAmount = "min_allowed_amount"
        case maxAllowedAmount = "max_allowed_amount"
        case recommendedMessage = "recommended_message"
        case installmentAmount = "installment_amount"
        case totalAmount = "total_amount"
    }
}

/*

 let decoder = JSONDecoder()
 let product = try decoder.decode(GroceryProduct.self, from: json)


 "installments": 24,
 "installment_rate": 30.86,
 "discount_rate": 0,
 "labels": [
 "CFT_30,96%|TEA_25,37%"
 ],
 "installment_rate_collector": [
 "MERCADOPAGO"
 ],
 "min_allowed_amount": 6,
 "max_allowed_amount": 250000,
 "recommended_message": "24 cuotas de $ 54,52 ($ 1.308,60)",
 "installment_amount": 54.52,
 "total_amount": 1308.6
 */
