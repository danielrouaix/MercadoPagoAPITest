//
//  UIImage+Network.swift
//  Pagos
//
//  Created by Daniel Dalto on 16/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import UIKit

let networkImageCache: NSCache = NSCache<NSURL,UIImage>()

extension UIImage {

    class func load(url: URL, completed: @escaping ((URL, UIImage?, Error?) -> Void)) {
        if let image = networkImageCache.object(forKey: url as NSURL) {
            completed(url, image, nil)
        } else {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                DispatchQueue.main.async {
                    if let data = data, let image = UIImage(data: data) {
                        networkImageCache.setObject(image, forKey: url as NSURL)
                        completed(url, image, error)
                    } else {
                        completed(url, nil, error)
                    }
                }
            }.resume()
        }
    }
}
