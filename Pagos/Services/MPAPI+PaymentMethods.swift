//
//  MPAPI+PaymentMethods.swift
//  Pagos
//
//  Created by Daniel Dalto on 15/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import Foundation

extension MPAPI {

    struct PaymentMethods {
        static let paymentMethods = "payment_methods"

        static func cardIssuers(paymentMethodId: String) -> String {
            return "payment_methods/card_issuers?payment_method_id=\(paymentMethodId)"
        }

        static func payerCosts(paymentMethodId: String, issuerId: String?, amount: Double) -> String {
            if let issuerId = issuerId {
                return "payment_methods/installments?amount=\(amount)&payment_method_id=\(paymentMethodId)&issuer.id=\(issuerId)"
            } else {
                return "payment_methods/installments?amount=\(amount)&payment_method_id=\(paymentMethodId)"
            }
        }
    }

    func getPaymentMethods(success: @escaping (([PaymentType]) -> Void), cached: (([PaymentType]) -> Void)?, failure: @escaping ((Error) -> Void)) -> URLSessionTask? {
        guard let url = pathToURL(path: PaymentMethods.paymentMethods) else {
            return nil
        }

        let task = get(url: url, success: { [weak self] (data) in
            do {
                let elements = try self?.jsonDecoder.decode([PaymentType].self, from: data)
                success(elements ?? [])
            }  catch let error as NSError {
                print(error.debugDescription)
                failure(error)
            }
        }, cached: { [weak self] (data) in
            do {
                let elements = try self?.jsonDecoder.decode([PaymentType].self, from: data)
                cached?(elements ?? [])
            }  catch let error as NSError {
                print(error.debugDescription)
                failure(error)
            }
        }, failure: failure)

        task?.resume()

        return task
    }

    func getCardIssuers(paymentMethodId: String, success: @escaping (([CardIssuer]) -> Void), cached: (([CardIssuer]) -> Void)?, failure: @escaping ((Error) -> Void)) -> URLSessionTask? {
        guard let url = pathToURL(path: PaymentMethods.cardIssuers(paymentMethodId: paymentMethodId)) else {
            return nil
        }

        let task = get(url: url, success: { [weak self] (data) in
            do {
                let elements = try self?.jsonDecoder.decode([CardIssuer].self, from: data)
                success(elements ?? [])
            }  catch let error as NSError {
                print(error.debugDescription)
                failure(error)
            }
            }, cached: { [weak self] (data) in
                do {
                    let elements = try self?.jsonDecoder.decode([CardIssuer].self, from: data)
                    cached?(elements ?? [])
                }  catch let error as NSError {
                    print(error.debugDescription)
                    failure(error)
                }
            }, failure: failure)

        task?.resume()

        return task
    }

    func getPayerCosts(paymentMethodId: String, issuerId: String?, amount: Double, success: @escaping ((PayerCosts) -> Void), cached: ((PayerCosts) -> Void)?, failure: @escaping ((Error) -> Void)) -> URLSessionTask? {
        guard let url = pathToURL(path: PaymentMethods.payerCosts(paymentMethodId: paymentMethodId, issuerId: issuerId, amount: amount)) else {
            return nil
        }

        let task = get(url: url, success: { [weak self] (data) in
            do {
                let element = try self?.jsonDecoder.decode([PayerCosts].self, from: data).first
                success(element!)
            }  catch let error as NSError {
                print(error.debugDescription)
                failure(error)
            }
            }, cached: { [weak self] (data) in
                do {
                    let element = try self?.jsonDecoder.decode([PayerCosts].self, from: data).first
                    cached?(element!)
                }  catch let error as NSError {
                    print(error.debugDescription)
                    failure(error)
                }
            }, failure: failure)

        task?.resume()

        return task
    }
}
