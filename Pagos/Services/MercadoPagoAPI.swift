//
//  MercadoPagoAPI.swift
//  Pagos
//
//  Created by Daniel Dalto on 15/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import Foundation

class MPAPI {

    static private let MPAPIPublicKey: String = "444a9ef5-8a6b-429f-abdf-587639155d88"
    static private let MPAPIBaseUrl: String = "https://api.mercadopago.com/v1/"

    static let shared = MPAPI(baseURL: MPAPI.MPAPIBaseUrl)

    let baseURL: String
    let jsonDecoder: JSONDecoder = JSONDecoder()
    let cache: NSCache = NSCache<NSURL,NSData>()

    private init(baseURL: String) {
        self.baseURL = baseURL
    }

    func pathToURL(path: String) -> URL? {
        let urlStr = MPAPI.MPAPIBaseUrl + path + (path.contains("?") ? "&public_key=\(MPAPI.MPAPIPublicKey)" : "?public_key=\(MPAPI.MPAPIPublicKey)")
        return URL(string: urlStr)
    }

    func get(url: URL, success: @escaping ((Data) -> Void), cached: ((Data) -> Void)?, failure: @escaping ((Error) -> Void)) -> URLSessionTask? {

        debugPrint("loading \(url)...")

        if let json = cache.object(forKey: url as NSURL) {
            cached?(json as Data)
        }

        let task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            do {
                if let error = error {
                    throw error
                }
                guard let data = data else {
                    throw NSError(domain: "MercadoPagoAPI", code: -1, userInfo: [NSLocalizedDescriptionKey: "No data returned."])
                }

                if (response as! HTTPURLResponse).statusCode != 200 {
                    let str = String(data: data, encoding: String.Encoding.utf8) as String?
                    debugPrint(str ?? "no data")
                    if let mpError = try self?.jsonDecoder.decode(MPError.self, from: data) {
                        throw mpError.stdError()
                    } else {
                        throw NSError(domain: "MercadoPagoAPI", code: (response as! HTTPURLResponse).statusCode, userInfo: [NSLocalizedDescriptionKey: "Response code error."])
                    }
                }
                DispatchQueue.main.async {
                    success(data)
                }
            } catch let error as NSError {
                print(error.debugDescription)
                DispatchQueue.main.async {
                    failure(error)
                }
            }
        }
        task.resume()

        return task
    }
}
