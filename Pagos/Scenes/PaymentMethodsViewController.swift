//
//  PaymentMethodsViewController.swift
//  Pagos
//
//  Created by Daniel Dalto on 15/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import UIKit

class PaymentMethodsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var amount: Double = 0

    @IBOutlet weak private var amountLabel: UILabel!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var loadingIndicator: UIActivityIndicatorView!

    private var currentTask: URLSessionTask?
    private var currentValues: [PaymentType] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "mpLogoOut"))

        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "es_AR")
        amountLabel.text = "Monto a pagar: " + currencyFormatter.string(from: NSNumber(floatLiteral: amount))!

        reloadPaymentMethods()

        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.register(UINib(nibName: "PaymentMethodsTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "paymentCell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        lastSelection = nil
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Load Data

    private func reloadPaymentMethods() {
        currentTask?.cancel()
        currentTask = MPAPI.shared.getPaymentMethods(success: { [weak self] (types) in
            self?.currentValues = types
            self?.tableView.reloadData()
            }, cached: { [weak self] (types) in
                if (self?.currentValues.count ?? 0) == 0 {
                    self?.currentValues = types
                    self?.tableView.reloadData()
                }
        }, failure: { (error) in
            debugPrint(error)
            let alert = UIAlertController(title: "Ups!", message: error.localizedDescription, preferredStyle: .actionSheet)
            let tryAgain = UIAlertAction(title: "Intentar nuevamente",
                                         style: .default,
                                         handler: { [weak self] (action) in
                                            self?.reloadPaymentMethods()
                                        })
            alert.addAction(tryAgain)
            let cancel = UIAlertAction(title: "Volver", style: .cancel, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
            self.tableView.reloadData()
        })
    }

    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentValues.count > 0 {
            loadingIndicator.isHidden = true
        } else {
            loadingIndicator.isHidden = (currentTask == nil || currentTask?.state != .running)
        }
        return currentValues.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as! PaymentMethodsTableViewCell

        let type = currentValues[indexPath.row]
        cell.nameLabel.text = type.name
        cell.imageUrl = type.thumbnail
        cell.placeholderImage = UIImage(named: type.paymentTypeId == "ticket" ? "paymentTicket" : "paymentCard")

        if let lastSelection = lastSelection, type == lastSelection {
            cell.accessoryType = .none
            let act = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            act.startAnimating()
            cell.accessoryView = act
        } else {
            cell.accessoryView = nil
            cell.accessoryType = .disclosureIndicator
        }

        return cell
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }

    var lastSelection: PaymentType?

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        let selection = currentValues[indexPath.row]
        lastSelection = selection
        showCardIssuers(for: selection)
    }
    
    // MARK: - Issuers check

    private func showCardIssuers(for payment: PaymentType) {
        currentTask?.cancel()
        tableView.reloadData()
        currentTask = MPAPI.shared.getCardIssuers(paymentMethodId: payment.paymentId!,
                                                  success: { [weak self] (issuers) in
                if issuers.count == 0 {
                    self?.performSegue(withIdentifier: "showInstallments", sender: payment)
                } else {
                    self?.performSegue(withIdentifier: "showCardIssuers", sender: payment)
                }
                                                    self?.lastSelection = nil
            },
            cached: nil,
            failure: { [weak self] (error) in
                debugPrint(error)
                let alert = UIAlertController(title: "Ups!", message: error.localizedDescription, preferredStyle: .actionSheet)
                let tryAgain = UIAlertAction(title: "Intentar nuevamente",
                                             style: .default,
                                             handler: { [weak self] (action) in
                                                self?.showCardIssuers(for: payment)
                })
                alert.addAction(tryAgain)
                let cancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action) in
                    self?.navigationController?.popViewController(animated: true)
                })
                alert.addAction(cancel)
                self?.present(alert, animated: true, completion: nil)
                self?.lastSelection = nil
        })
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCardIssuers" {
            if let vc = segue.destination as? CardIssuerViewController {
                vc.amount = self.amount
                vc.paymentType = sender as! PaymentType
            }
        }
        if segue.identifier == "showInstallments" {
            if let vc = segue.destination as? InstallmentsViewController {
                vc.amount = self.amount
                vc.paymentType = sender as! PaymentType
            }
        }
    }


}
