//
//  TicketViewController.swift
//  Pagos
//
//  Created by Daniel Dalto on 16/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import UIKit

class TicketViewController: UIViewController {

    @IBOutlet private weak var paymentLogoImageView: UIImageView!
    @IBOutlet private var issuerLogoImageView: UIImageView!
    @IBOutlet private weak var paymentTypeLabel: UILabel!
    @IBOutlet private var issuerLabel: UILabel!
    @IBOutlet private weak var baseAmountLabel: UILabel!
    @IBOutlet private weak var costLabel: UILabel!
    @IBOutlet private weak var cftLabel: UILabel!
    @IBOutlet private weak var totalAmountLabel: UILabel!

    private var payment: PaymentType!
    private var issuer: CardIssuer?
    private var cost: Installment!
    private var baseAmount: Double!

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    init(amount: Double, payment pay: PaymentType, issuer iss: CardIssuer?, cost cos: Installment) {
        payment = pay
        issuer = iss
        cost = cos
        baseAmount = amount

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func loadImageFromUrl(url: URL?, in imageView: UIImageView, placeholder: UIImage?) {
        imageView.image = placeholder
        if let imageUrl = url {
            UIImage.load(url: imageUrl) { (url, image, error) in
                if let image = image {
                    imageView.image = image
                } else {
                    imageView.image = placeholder
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadImageFromUrl(url: payment.thumbnail,
                         in: paymentLogoImageView,
                         placeholder: (payment.paymentTypeId == "ticket") ? UIImage(named: "paymentTicket") : UIImage(named: "paymentCard"))
        paymentTypeLabel.text = payment.name

        if let issuer = issuer {
            loadImageFromUrl(url: issuer.thumbnail,
                             in: issuerLogoImageView,
                             placeholder: UIImage(named: "bankGeneric"))
            issuerLabel.text = issuer.name
        } else {
            paymentLogoImageView.contentMode = .center
            issuerLogoImageView.removeFromSuperview()
            issuerLabel.removeFromSuperview()
        }

        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "es_AR")
        
        baseAmountLabel.text = "Precio de lista " + currencyFormatter.string(from: NSNumber(floatLiteral: baseAmount))!
        costLabel.text = cost.recommendedMessage
        cftLabel.text = cost.cft
        totalAmountLabel.text = currencyFormatter.string(from: NSNumber(floatLiteral: cost.totalAmount ?? baseAmount))
    }

    @IBAction private func readyAction() {
        self.dismiss(animated: true, completion: nil)
    }

}
