//
//  InitialViewController.swift
//  Pagos
//
//  Created by Daniel Dalto on 15/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    @IBOutlet fileprivate weak var colorLogoImageView: UIImageView!
    @IBOutlet fileprivate var dropColorTopLC: NSLayoutConstraint!
    @IBOutlet fileprivate var dropView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        dropView.layer.cornerRadius = 5
        dropView.clipsToBounds = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if animated {
            startAnimation()
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.startAnimation()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Animation Sequence

    private func startAnimation() {
        dropColorTopLC.isActive = false
        UIView.animate(withDuration: 1,
                       delay: 0.3,
                       options: [.curveEaseIn],
                       animations: {
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.showColorLogo()
        }
    }

    private func showColorLogo() {
        UIView.animate(withDuration: 0.5,
                       animations: {
                        self.dropView.alpha = 0
                        self.dropView.transform = CGAffineTransform(scaleX: 40, y: 40)
        })

        UIView.animate(withDuration: 0.4,
                       delay: 0.3,
                       options: [],
                       animations: {
                        self.colorLogoImageView.alpha = 1
        },
                       completion: { (finished) in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            self.performSegue(withIdentifier: "showAmount", sender: nil)
                        })
        })
    }

}
