//
//  CardIssuerTableViewCell.swift
//  Pagos
//
//  Created by Daniel Dalto on 16/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import UIKit

class CardIssuerTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!

    var imageUrl: URL? {
        didSet {
            self.logoImageView.image = UIImage(named: "bankGeneric")
            if let imageUrl = imageUrl {
                UIImage.load(url: imageUrl) { [weak self] (url, image, error) in
                    if url == imageUrl {
                        if let image = image {
                            self?.logoImageView.image = image
                        } else {
                            self?.logoImageView.image = UIImage(named: "bankGeneric")
                        }
                    }
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        nameLabel.text = nil
        logoImageView.image = nil
        imageUrl = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
