//
//  CardIssuerViewController.swift
//  Pagos
//
//  Created by Daniel Dalto on 16/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import UIKit

class CardIssuerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var amount: Double = 0
    var paymentType: PaymentType!

    @IBOutlet weak private var amountLabel: UILabel!
    @IBOutlet weak private var paymentTypeLabel: UILabel!
    @IBOutlet weak private var tableView: UITableView!

    private var currentTask: URLSessionTask?
    private var currentValues: [CardIssuer] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "mpLogoOut"))

        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "es_AR")
        amountLabel.text = "Monto a pagar: " + currencyFormatter.string(from: NSNumber(floatLiteral: amount))!
        paymentTypeLabel.text = paymentType.name ?? "¿Paga Dios?"

        reloadCardIssuers()

        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.register(UINib(nibName: "CardIssuerTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "issuerCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Load Data

    private func reloadCardIssuers() {
        currentTask?.cancel()
        currentTask = MPAPI.shared.getCardIssuers(paymentMethodId: paymentType.paymentId ?? "", success: { [weak self] (issuers) in
            self?.currentValues = issuers
            self?.tableView.reloadData()
            if issuers.count == 0 {
                self?.performSegue(withIdentifier: "showInstallments", sender: nil)
            }
            }, cached: { [weak self] (issuers) in
                if (self?.currentValues.count ?? 0) == 0 {
                    self?.currentValues = issuers
                    self?.tableView.reloadData()
                }
            }, failure: { (error) in
                debugPrint(error)
                let alert = UIAlertController(title: "Ups!", message: error.localizedDescription, preferredStyle: .actionSheet)
                let tryAgain = UIAlertAction(title: "Intentar nuevamente",
                                             style: .default,
                                             handler: { [weak self] (action) in
                                                self?.reloadCardIssuers()
                })
                alert.addAction(tryAgain)
                let cancel = UIAlertAction(title: "Volver", style: .cancel, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
                alert.addAction(cancel)
                self.present(alert, animated: true, completion: nil)
        })
    }

    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentValues.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "issuerCell", for: indexPath) as! CardIssuerTableViewCell

        let type = currentValues[indexPath.row]
        cell.nameLabel.text = type.name
        cell.imageUrl = type.thumbnail

        return cell
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        let issuer = currentValues[indexPath.row]
        self.performSegue(withIdentifier: "showInstallments", sender: issuer)
    }
    


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showInstallments" {
            if let vc = segue.destination as? InstallmentsViewController {
                vc.amount = self.amount
                vc.paymentType = paymentType
                vc.cardIssuer = sender as? CardIssuer
            }
        }
    }
    

}
