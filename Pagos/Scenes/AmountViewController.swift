//
//  AmountViewController.swift
//  Pagos
//
//  Created by Daniel Dalto on 15/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import UIKit

class AmountViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet fileprivate var amountTextField: UITextField!

    fileprivate let currencyFormatter = NumberFormatter()
    fileprivate let validChars = Set("1234567890,")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: nil, style: .done, target: nil, action: nil)
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "mpLogoOut"))
        
        self.navigationController?.isNavigationBarHidden = false

        self.amountTextField.text = ""

        // initiate currency formatter
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .decimal
        currencyFormatter.locale = Locale(identifier: "es_AR")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        amountTextField.becomeFirstResponder()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        amountTextField.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Text Field Delegation

    func filteredAmountString(from: String) -> String {
        return String(from.filter { validChars.contains($0) })
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

        textField.text = filteredAmountString(from:textField.text!)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text as NSString? {
            var txtAfterUpdate = text.replacingCharacters(in: range, with: string)
            txtAfterUpdate = filteredAmountString(from: txtAfterUpdate)
            if let _ = currencyFormatter.number(from: txtAfterUpdate) {
                textField.text = txtAfterUpdate
            } else if txtAfterUpdate.isEmpty {
                textField.text = ""
            }
            return false
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        textField.resignFirstResponder()

        if let text = textField.text {

            if let number = currencyFormatter.number(from: filteredAmountString(from: text))?.doubleValue, let priceString = currencyFormatter.string(from: NSNumber(floatLiteral: number)) {
                textField.text = priceString

                if number > 0 {
                    // Go to next screen
                    goNext()
                }
            }
        }

        return true
    }

    // MARK: - Actions

    func cleanup() {
        amountTextField.text = ""
    }

    @IBAction func goNext() {
        if let text = amountTextField.text {
            if let number = currencyFormatter.number(from: filteredAmountString(from: text))?.doubleValue, number > 0 {
                self.performSegue(withIdentifier: "selectPaymentMethod", sender: number)
            }
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectPaymentMethod" {
            if let vc = segue.destination as? PaymentMethodsViewController {
                vc.amount = (sender as? Double) ?? 0
            }
        }
    }

}
