//
//  InstallmentsTableViewCell.swift
//  Pagos
//
//  Created by Daniel Dalto on 16/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import UIKit

class InstallmentsTableViewCell: UITableViewCell {

    @IBOutlet weak var recomendedMessage: UILabel!
    @IBOutlet weak var cftMessage: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        recomendedMessage.text = nil
        cftMessage.text = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
