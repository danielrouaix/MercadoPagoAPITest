//
//  InstallmentsViewController.swift
//  Pagos
//
//  Created by Daniel Dalto on 16/4/18.
//  Copyright © 2018 mercadopago. All rights reserved.
//

import UIKit

class InstallmentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var amount: Double = 0
    var paymentType: PaymentType!
    var cardIssuer: CardIssuer?

    @IBOutlet weak private var amountLabel: UILabel!
    @IBOutlet weak private var paymentTypeLabel: UILabel!
    @IBOutlet weak private var cardIssuerLabel: UILabel!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var loadingIndicator: UIActivityIndicatorView!

    private var currentTask: URLSessionTask?
    private var payerCosts: PayerCosts?
    private var currentValues: [Installment] {
        return payerCosts?.costs ?? []
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "mpLogoOut"))

        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "es_AR")
        amountLabel.text = "Monto a pagar: " + currencyFormatter.string(from: NSNumber(floatLiteral: amount))!
        paymentTypeLabel.text = paymentType.name ?? "¿Paga Dios?"
        cardIssuerLabel.text = cardIssuer?.name ?? ""

        reloadInstallments()

        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.register(UINib(nibName: "InstallmentsTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "InstallmentsCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Load Data

    private func reloadInstallments() {
        currentTask?.cancel()
        currentTask = MPAPI.shared.getPayerCosts(paymentMethodId: paymentType.paymentId ?? "",
                                                 issuerId: cardIssuer?.cardIssuerId,
                                                 amount: amount,
                                                 success: { [weak self] (costs) in
            self?.payerCosts = costs
            self?.tableView.reloadData()
            }, cached: { [weak self] (costs) in
                if (self?.currentValues.count ?? 0) == 0 {
                    self?.payerCosts = costs
                    self?.tableView.reloadData()
                }
            }, failure: { (error) in
                debugPrint(error)
                let alert = UIAlertController(title: "Ups!", message: error.localizedDescription, preferredStyle: .actionSheet)
                let tryAgain = UIAlertAction(title: "Intentar nuevamente",
                                             style: .default,
                                             handler: { [weak self] (action) in
                                                self?.reloadInstallments()
                })
                alert.addAction(tryAgain)
                let cancel = UIAlertAction(title: "Volver", style: .cancel, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
                alert.addAction(cancel)
                self.present(alert, animated: true, completion: nil)
        })
    }

    // MARK: - Return to amount

    private func returnToAmount(with cost: Installment) {

        let ticket = TicketViewController(amount: amount,
                                          payment: paymentType,
                                          issuer: cardIssuer,
                                          cost: cost)

        self.navigationController?.present(ticket, animated: true, completion: {
            var controller: AmountViewController?
            self.navigationController?.viewControllers.forEach({ (ctrl) in
                if ctrl is AmountViewController {
                    controller = ctrl as? AmountViewController
                }
            })
            if let controller = controller {
                self.navigationController?.popToViewController(controller, animated: false)
            }
        })
    }

    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentValues.count > 0 {
            loadingIndicator.isHidden = true
        } else {
            loadingIndicator.isHidden = (currentTask == nil || currentTask?.state != .running)
        }
        return currentValues.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InstallmentsCell", for: indexPath) as! InstallmentsTableViewCell

        let cost = currentValues[indexPath.row]
        cell.recomendedMessage.text = cost.recommendedMessage
        var labels: [String] = cost.labels ?? []
        if let index = labels.index(of: "recommended_installment") {
            labels.remove(at: index)
        }
        cell.cftMessage.text = cost.cft

        return cell
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        let cost = currentValues[indexPath.row]
        self.returnToAmount(with: cost)
    }
    

}
